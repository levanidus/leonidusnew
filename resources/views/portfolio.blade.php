<html>
<head>
  <meta charset="utf-8">
  <title>Портфолио</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
  <link href="{{ asset("css/bootstrap.min.css") }}" rel="stylesheet">
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="/js/ie/html5shiv.js"></script>
    <script src="/js/ie/respond.min.js"></script>
  <![endif]-->
</head>
<body>
<div class="container-fluid">
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="/">LEONIDUS.RU</a>
      </div>  
    </div>
  </nav>
  <div class="row">  
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h1>Портфолио</h1>
      <h3>Знаю PHP Laravel HTML CSS MySQL PostgreSQL Ajax Jquery шаблоны Smarty
      <br><br>
      Реализованы следующие проекты</h3>  
    </div>
  </div>
  
  <!-- FIRST ROW -->  
  
  <div class="row">  
    <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
      <h5>Сервис учёта личных финансов - <a href="https://simpfi.com/ru" target="_blank">Simpfi</a></h5>
      Самый простой способ учитывать личные финансы
      <br>
      Simpfi экономит время и делает процесс ввода личных финансовых данных удобным 
    </div>
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
    </div>
    <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
      <h5>Агрегатор предложений рынка электроники - <a href="http://zakupi.net" target="_blank">Zakupi.net</a></h5>
      Поиск по дистрибьюторам Zakupi.net обеспечивает стабильный и моментальный доступ к актуальной информации по наличию товаров на складах поставщиков. 
      <br><br>      
      В поисковую систему включены постоянные обновления данных по наличию более 1 500 000 товаров от 73 дистрибьюторов со складами на территории Российской Федерации. 
    </div>
  </div>   
  
  <!-- SECOND ROW -->  
   
  <div class="row">  
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <br><br>
    </div>
  </div>  
  <div class="row">  
    <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">	  
      <h5>Сайт агентства недвижимости Маклер - <a href="http://makler74.ru" target="_blank">www.makler74.ru</a></h5>      
      Возможности
      <ul>
        <li>Добавление обьявлений о недвижимости сотрудниками агентства</li>
        <li>Регистрация агентств недвижимости</li>
        <li>Четыре роли пользователей - суперпользователь, агент, агентство, контент-менеджер</li>
        <li>Рекламная сеть - возможность добавлять баннеры на сайт</li>
      </ul>     
    </div>
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
    </div>
    <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
      <h5>Личный сайт <a href="http://leonidus.ru">Leonidus.ru</a></h5>
      Сделан на фреймворке Laravel 5, база данных - PostgreSQL        
    </div>
  </div>  
    
  <!-- THIRD ROW -->
  
  <div class="row">  
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <br><br>
    </div>
  </div>
  <div class="row">  
    <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
      <h5>Проект Сервис поиска исполнителей (ремонтных бригад)</h5>
      Заказчики проекта предполагали организовать сервис по поиску ремонтых бригад - то есть клиент заходя на сайт имеет возможность выбрать бригаду для ремонта исходя из стоимости, фотографий сделанных обьектов, количества работников и прочих факторов
      <br><br>
      После выбора бригады сервис продаёт заказчику контакты выбранной фирмы и на этом зарабатывает деньги
      <br><br> 
      Проект не был запущен ввиду потери связи с заказчиком
      <br><br>
      Возможности
      <br>
      <ul>            
        <li>Регистрация подрядчика</li>
        <li>Регистрация клиента</li>
        <li>Загрузка фотографий обьектов для подрядчика</li>
        <li>Возможность для клиента оставлять заявку на выбранную фирму из личного кабинета</li>
        <li>Личный кабинет администратора сервиса - редактирование подрядчиков, заказчиков, фотографий, комментариев, заявок</li>
      </ul> 
    </div>
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
    </div>
    <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
      <h5>Стартап Сервис хранения юридических документов<br><br><a href="http://jurdocs.leonidus.ru" target="_blank">jurdocs.leonidus.ru</a></h5>
      Задумка данного проекта - дать возможность юристам и юридическим компаниям хранить на удалённом сервере информацию о делах, а также хранить сканы решений судов, сканы договоров и файлы (также решения судов и файлы)
      <br><br> 
      Данный проект был реализован в расчёте на высокие нагрузки - изначально предуматривалось хранение информации для каждого пользователя в отдельной базе на разных серверах
      <br><br>
      При соответствующей доработке данный проект может быть использован как средство хранения любой графической и файловой информации для организаций с разбивкой по документам/делам и прочее 
    </div>
  </div>
  
  <!-- FOURTH ROW -->
  
  <div class="row">  
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <br><br>
    </div>
  </div>
  <div class="row">  
    <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
      <h5>Автоматизация работы архива МУП Архитектурно-планировочный центр администрации г. Челябинска</h5>
      Возможности
      <ul>
        <li>Добавление проекта планировки</li>
        <li>Контроль выдачи проектов сотрудникам организации с сохранением даты и времени выдачи</li>
        <li>Добавление описи проекта - пояснительная записка,альбомы,свидетельства</li>
        <li>Поиск проектов - по номеру,заключению,году а также по названию - по названию поиск реализован на Sphinx</li>
        <li>Статистика - кому и в какой период выдавались проекты</li>
      </ul> 
      Проект недоступен в сети Интернет - работает во внутренней сети МУП (реализована сеть и сервер на Ubuntu Linux)
    </div>
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
    </div>   
    <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
      <h5>Также были в разработке</h5>
      <br>
      <ul>
        <li>Сайт букмекерской конторы</li>
        <li>Сайт магазина запчастей</li>
      </ul>
      <br>
      Кроме того занимался разработкой парсеров - как в плане парсинга цен (e96.ru idei74.ru nord24.ru svyaznoy.ru и прочие), так и в плане парсинга изображений, инструкций и свидетельств (mvideo.ru и eldorado.ru)
      <br>
    </div>
</div>  	
</body>
</html>
