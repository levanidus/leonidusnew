<html>
<head>
  <meta charset="utf-8">
  <title>@yield('title')</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
  <meta name="robots" content="index,follow">
  <meta name="description" content="@yield('title')">
  <meta name="keywords" content="прикол, анекдот, фотографии, разработка, сайт, Челябинск, креатив, блог, личный дневник">
  <link href="{{ asset("css/bootstrap.min.css") }}" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script type="text/javascript" src="{{ asset("js/months.js") }}"></script>
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="/js/ie/html5shiv.js"></script>
    <script src="/js/ie/respond.min.js"></script>
  <![endif]-->
</head>
<body>
<div class="container-fluid">
  <nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="/">LEONIDUS.RU</a>
    </div>  
  </div>
</nav>
  <div class="row">  
    <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">	  
      @yield('content')
    </div>
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">	  
      <div class="panel panel-danger">
        <div class="panel-heading"><b>АРХИВ</b></div>
        <div class="panel-body">
          @include('itemclient/months')
        </div>
      </div>
    </div>
  </div>  
</div>  
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-62949210-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
