<html>
<head>
  <meta charset="utf-8">
  <title>LEONIDUS.RU - Управление</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
  <script src="//code.jquery.com/jquery-1.9.1.min.js"></script> 
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.1/css/bootstrap.min.css" rel="stylesheet"> 
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.1/js/bootstrap.min.js"></script> 
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
  <script src="{{ asset("js/summernote.min.js") }}"></script>
  <link href="{{ asset("css/summernote.css") }}" rel="stylesheet">
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="/js/ie/html5shiv.js"></script>
    <script src="/js/ie/respond.min.js"></script>
  <![endif]-->
</head>
<body>
<div class="container-fluid">
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="/goadmin/items">LEONIDUS.RU - Управление</a>
      </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li><a href="/goadmin/items">Список постов</a></li>
          <li><a href="/goadmin/items/add">Добавить пост</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li><a href="/getadmin/logout">Выход</a></li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="row">  
    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
      &nbsp;
    </div>
    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">	  
      @yield('content')  
    </div>
    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
      &nbsp;
    </div>
  </div>  
</div>  	
</body>
</html>