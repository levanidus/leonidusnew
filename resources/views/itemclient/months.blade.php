<h4><a href="#" id="year_2015" class="year">2015</a></h4>
<span id="months_2015" style="display: none;">
<a href="/archive/2015/04">Апрель</a>
<a href="/archive/2015/05">Май</a>
</span>
<h4><a href="#" id="year_2014" class="year">2014</a></h4>
<span id="months_2014" style="display: none;">
<a href="/archive/2014/01">Январь</a>
<a href="/archive/2014/03">Март</a>
<a href="/archive/2014/06">Июнь</a>
</span>
<h4><a href="#" id="year_2013" class="year">2013</a></h4>
<span id="months_2013" style="display: none;">
<a href="/archive/2013/01">Январь</a>
<a href="/archive/2013/02">Февраль</a>
<a href="/archive/2013/03">Март</a>
<a href="/archive/2013/04">Апрель</a>
<a href="/archive/2013/05">Май</a>
<a href="/archive/2013/06">Июнь</a>
<a href="/archive/2013/07">Июль</a>
<a href="/archive/2013/08">Август</a>
<a href="/archive/2013/10">Октябрь</a>
<a href="/archive/2013/11">Ноябрь</a>
<a href="/archive/2013/12">Декабрь</a>
</span>
<h4><a href="#" id="year_2012" class="year">2012</a></h4>
<span id="months_2012" style="display: none;">
<a href="/archive/2012/01">Январь</a>
<a href="/archive/2012/02">Февраль</a>
<a href="/archive/2012/03">Март</a>
<a href="/archive/2012/04">Апрель</a>
<a href="/archive/2012/05">Май</a>
<a href="/archive/2012/06">Июнь</a>
<a href="/archive/2012/07">Июль</a>
<a href="/archive/2012/08">Август</a>
<a href="/archive/2012/09">Сентябрь</a>
<a href="/archive/2012/10">Октябрь</a>
<a href="/archive/2012/11">Ноябрь</a>
<a href="/archive/2012/12">Декабрь</a>
</span>
<h4><a href="#" id="year_2011" class="year">2011</a></h4>
<span id="months_2011" style="display: none;">
<a href="/archive/2011/01">Январь</a>
<a href="/archive/2011/02">Февраль</a>
<a href="/archive/2011/03">Март</a>
<a href="/archive/2011/04">Апрель</a>
<a href="/archive/2011/05">Май</a>
<a href="/archive/2011/06">Июнь</a>
<a href="/archive/2011/07">Июль</a>
<a href="/archive/2011/08">Август</a>
<a href="/archive/2011/09">Сентябрь</a>
<a href="/archive/2011/10">Октябрь</a>
<a href="/archive/2011/11">Ноябрь</a>
<a href="/archive/2011/12">Декабрь</a>
</span>
<h4><a href="#" id="year_2010" class="year">2010</a></h4>
<span id="months_2010" style="display: none;">
<a href="/archive/2010/01">Январь</a>
<a href="/archive/2010/02">Февраль</a>
<a href="/archive/2010/03">Март</a>
<a href="/archive/2010/04">Апрель</a>
<a href="/archive/2010/05">Май</a>
<a href="/archive/2010/06">Июнь</a>
<a href="/archive/2010/07">Июль</a>
<a href="/archive/2010/08">Август</a>
<a href="/archive/2010/09">Сентябрь</a>
<a href="/archive/2010/10">Октябрь</a>
<a href="/archive/2010/11">Ноябрь</a>
<a href="/archive/2010/12">Декабрь</a>
</span>
<h4><a href="#" id="year_2009" class="year">2009</a></h4>
<span id="months_2009" style="display: none;">
<a href="/archive/2009/01">Январь</a>
<a href="/archive/2009/02">Февраль</a>
<a href="/archive/2009/03">Март</a>
<a href="/archive/2009/04">Апрель</a>
<a href="/archive/2009/05">Май</a>
<a href="/archive/2009/06">Июнь</a>
<a href="/archive/2009/07">Июль</a>
<a href="/archive/2009/08">Август</a>
<a href="/archive/2009/09">Сентябрь</a>
<a href="/archive/2009/10">Октябрь</a>
<a href="/archive/2009/11">Ноябрь</a>
<a href="/archive/2009/12">Декабрь</a>
</span>
<h4><a href="#" id="year_2008" class="year">2008</a></h4>
<span id="months_2008" style="display: none;">
<a href="/archive/2008/04">Апрель</a>
<a href="/archive/2008/05">Май</a>
<a href="/archive/2008/06">Июнь</a>
<a href="/archive/2008/07">Июль</a>
<a href="/archive/2008/08">Август</a>
<a href="/archive/2008/09">Сентябрь</a>
<a href="/archive/2008/10">Октябрь</a>
<a href="/archive/2008/11">Ноябрь</a>
<a href="/archive/2008/12">Декабрь</a>
</span>
