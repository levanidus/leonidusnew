@extends('app')

@section('title')
  {!! $title !!}  
@stop

@section('content')
<div align="center"><?php echo $items->render(); ?></div>
@if (count($items)>0)
  @foreach($items as $item)
    <div class="panel panel-default">
      <div class="panel-body">	
        <h3>{!! $item->itemtitle !!}</h3>
        <hr>
        {!! $item->itembody !!}
        <div align="right"><b>{!! $item->created_at->format('d.m.Y') !!}</b></div>
      </div>
    </div>  
  @endforeach
@else
  <h3>Упс ничего нету - похоже никто ничего не написал</h3>
@endif   
<div align="center"><?php echo $items->render(); ?></div>     
@stop