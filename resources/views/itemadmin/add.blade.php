@extends('goadmin')

@section('content')
  <script>
    $(document).ready(function() {
      $('#summernote').summernote({
         height: 250,      
      });
    });
  </script>

  <h3>Добавить новый пост</h3>
  
  <hr>

  @if ($errors->any())
    <ul>
      @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  @endif

  {!! Form::open(['url' => 'goadmin/items']) !!}
    @include('itemadmin.form',['sbtext' => 'ДОБАВИТЬ ПОСТ'])
  {!! Form::close() !!}
  
@stop