@extends('goadmin')

@section('content')

   <script>
    $(document).ready(function() {
      $('#summernote').summernote({
         height: 250,      
      });
    });
  </script>

  <h3>Редактировать пост {{ $item->itemtitle }}</h3>
  
  <hr>

  {!! Form::model($item, ['method' => 'PATCH','action' => ['ItemsController@update',$item->id]]) !!}
    @include('itemadmin.form',['sbtext' => 'СОХРАНИТЬ ПОСТ'])
  {!! Form::close() !!}
  

@stop