@extends('goadmin')

@section('content')

  <h3>Список постов</h3>
  
  <hr>
  
  <table class="table table-bordered">
    <tr>
      <th>Заголовок</th>
      <th>Редактировать</th>
      <th>Удалить</th>
    </tr>  
    @foreach($items as $item)
    <tr>
      <td>{{ $item->itemtitle }}</td>
      <td><a href="{{ url('goadmin/items/edit',$item->id) }}">Редактировать</a></td>
      <td><a href="{{ url('goadmin/items/delete',$item->id) }}">Удалить</a></td>
    </tr>
    @endforeach
  </table>

  <?php echo $items->render(); ?>
@stop