<div class="form-group">
  {!! Form::label('itemtitle','Заголовок:') !!}
    
  {!! Form::text('itemtitle',null,['class' => 'form-control']) !!}
</div>  
  
<div class="form-group">
  {!! Form::label('itembody','Содержание:') !!}
       
  {!! Form::textarea('itembody',null,['class' => 'form-control','id' => 'summernote']) !!} 
</div>
    
<div class="form-group">  
  {!! Form::submit($sbtext,['class' => 'btn btn-primary form-control']) !!}  
</div>