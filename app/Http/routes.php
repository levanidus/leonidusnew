<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/','ItemsController@index');

Route::get('archive/{year}/{month}', 'ItemsController@archive');

Route::get('portfolio','ItemsController@portfolio');

Route::get('goadmin/items','ItemsController@goadminindex');

Route::post('goadmin/items','ItemsController@store');

Route::patch('goadmin/items/{id}','ItemsController@update');

Route::get('goadmin/items/add', 'ItemsController@add');

Route::get('goadmin/items/edit/{id}','ItemsController@edit');

Route::get('goadmin/items/delete/{id}','ItemsController@delete');

Route::controllers([
	'getadmin' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
