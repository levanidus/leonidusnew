<?php namespace App\Http\Controllers;

use App\Item;
use App\Http\Requests;
use App\Http\Requests\CreateItemRequest; 
use App\Http\Controllers\Controller;

use Request;

class ItemsController extends Controller {
	
	public function __construct() {
		
	  $this->middleware('auth',['except' => ['index','archive','portfolio']]);	
	}

	public function index() {	
		
	  $title = 'LEONIDUS.RU';	
			
	  $items = Item::whereBetween('created_at',[date('Y-m').'-01',date('Y-m-d',strtotime(date('Y-m-d'))+24*60*60)])
	                 ->orderBy('id','desc')   
	                 ->paginate(5);	

     return view('itemclient.index',compact('items','title')); 		
		
	}
	
	public function archive($year,$month) {
		
     $months = array('Январь','Февраль','Март','Апрель','Май','Июнь',
                     'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь');
                     
     $monthdigit = (int) $month;                 		
		
	  $title = 'LEONIDUS.RU - Архив за '.$months[$monthdigit-1].' '.$year.' года';	
		
	  $daysinmonth = cal_days_in_month(CAL_GREGORIAN, $monthdigit, (int) $year);	
		
	  $items = Item::whereBetween('created_at',[$year.'-'.$month.'-01',$year.'-'.$month.'-'.$daysinmonth])
	                 ->orderBy('id','desc')   
	                 ->paginate(5);    	

     return view('itemclient.index',compact('items','title')); 		
		
	}
	
	public function goadminindex() {
		
	  $items = Item::latest()->paginate(10);
	  
	  return view('itemadmin.index',compact('items'));	
	}
	
	public function add() {

     return view('itemadmin.add');
		
	}
	
	public function store(CreateItemRequest $request) {
	  
	  Item::create($request->all());	
	  
	  return redirect('goadmin/items');
		
	}
	
	public function edit($id) {
	  
	  $item = Item::findOrFail($id);	
	  
	  return view('itemadmin.edit',compact('item'));
	}

   public function update($id) {
   	
     $item = Item::findOrFail($id);
     
     $item->update(Request::all());
     
     return redirect('goadmin/items');
   	
   }
   
   public function delete($id)
   {
     $item = Item::findOrFail($id);

     $item->delete();
     
     return redirect('goadmin/items');   
   }
   
   public function portfolio() {

     return view('portfolio');
		
	}
   
}
